package cheekymate;

import java.util.ArrayList;

import cheekymate.Board.CheekyBoard;
import cheekymate.Board.ChessBoard;
import cheekymate.Board.GameState;
import cheekymate.Figures.Colour;
import cheekymate.Figures.Figure;
import cheekymate.utils.FigureType;
import cheekymate.utils.Move;
import computer.Computer;
import user_interface.Window;

public class GameController {
	
	public ArrayList<ChessBoard> chessBoardHistory;
	
	private Window window;
	
	GameController() throws Exception {
		this.window = window;
		
		Figure[][] board = {
				 {Figure.getFigureInstance(FigureType.Rook,Colour.black), Figure.getFigureInstance(FigureType.Knight,Colour.black), Figure.getFigureInstance(FigureType.Bishop,Colour.black), Figure.getFigureInstance(FigureType.Queen,Colour.black), Figure.getFigureInstance(FigureType.King,Colour.black), Figure.getFigureInstance(FigureType.Bishop,Colour.black), Figure.getFigureInstance(FigureType.Knight,Colour.black), Figure.getFigureInstance(FigureType.Rook,Colour.black)},
				 {Figure.getFigureInstance(FigureType.Pawn,Colour.black), Figure.getFigureInstance(FigureType.Pawn,Colour.black), Figure.getFigureInstance(FigureType.Pawn,Colour.black), Figure.getFigureInstance(FigureType.Pawn,Colour.black), Figure.getFigureInstance(FigureType.Pawn,Colour.black), Figure.getFigureInstance(FigureType.Pawn,Colour.black), Figure.getFigureInstance(FigureType.Pawn,Colour.black), Figure.getFigureInstance(FigureType.Pawn,Colour.black)},
				 {null,null,null,null,null,null,null,null},
				 {null,null,null,null,null,null,null,null},
				 {null,null,null,null,null,null,null,null},
				 {null,null,null,null,null,null,null,null},
				 {Figure.getFigureInstance(FigureType.Pawn,Colour.white), Figure.getFigureInstance(FigureType.Pawn,Colour.white), Figure.getFigureInstance(FigureType.Pawn,Colour.white), Figure.getFigureInstance(FigureType.Pawn,Colour.white), Figure.getFigureInstance(FigureType.Pawn,Colour.white), Figure.getFigureInstance(FigureType.Pawn,Colour.white), Figure.getFigureInstance(FigureType.Pawn,Colour.white), Figure.getFigureInstance(FigureType.Pawn,Colour.white)},
				 {Figure.getFigureInstance(FigureType.Rook,Colour.white), Figure.getFigureInstance(FigureType.Knight,Colour.white), Figure.getFigureInstance(FigureType.Bishop,Colour.white), Figure.getFigureInstance(FigureType.Queen,Colour.white), Figure.getFigureInstance(FigureType.King,Colour.white), Figure.getFigureInstance(FigureType.Bishop,Colour.white), Figure.getFigureInstance(FigureType.Knight,Colour.white), Figure.getFigureInstance(FigureType.Rook,Colour.white)}
		};
		
		chessBoardHistory = new ArrayList<ChessBoard>();
		chessBoardHistory.add(new CheekyBoard(board, Colour.white, null, 0));		
	}
	
	public void setWindow(Window window) {
		this.window = window;
	}
	
	void updateThreeFoldRepetition(ChessBoard newBoard) {
		byte count = 0;
		//Check for three fold repetition
		for (ChessBoard board : chessBoardHistory) {
			if (newBoard.equals(board))
				count++;
		}
		if (count >= 3)
			newBoard.setDraw();
	}
	
	public boolean tryMove(Move m) throws Exception{
		ChessBoard newBoard = chessBoardHistory.get(chessBoardHistory.size() - 1).doMove(m);
		if (newBoard == null)
			return false;
		
		updateThreeFoldRepetition(newBoard);
		
		chessBoardHistory.add(newBoard);
		
		//Starts computer, which in turn calls tryComputerMove until succession
		Computer c = new Computer();
		c.init(this, 5000);//5 seconds to think
		c.start();
		
		return true;
	}
	
	public boolean tryComputerMove(Move m) throws Exception{
		ChessBoard newBoard = chessBoardHistory.get(chessBoardHistory.size() - 1).doMove(m);
		if (newBoard == null)
			return false;
		
		updateThreeFoldRepetition(newBoard);
		
		chessBoardHistory.add(newBoard);
		window.setInteractive(true);
		window.updateDisplay();
		return true;
	}
	
	
}
