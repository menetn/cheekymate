package cheekymate;


import cheekymate.GameController;
import cheekymate.utils.Move;
import user_interface.CommandLineInterface;
import user_interface.Window;

public class Main {

	static GameController gc;
	static Window window;
	
	public static void main(String[] args) throws Exception{
		init();
		
	}
	
	public static void init() throws Exception {
		gc = new GameController();
		window = new Window(gc);
		gc.setWindow(window);
		window.updateDisplay();
	}
}