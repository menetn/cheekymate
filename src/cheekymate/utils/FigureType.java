package cheekymate.utils;

public enum FigureType {
	King,
	Queen,
	Bishop,
	Knight,
	Rook,
	Pawn
}
