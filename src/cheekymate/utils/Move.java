package cheekymate.utils;

public class Move {
	public Coordinate start;
	public Coordinate end;
		
	public Move(Coordinate start, Coordinate end){
		this.start = start;
		this.end = end;
	}
	
	public Move(byte start_row, byte start_column, byte end_row, byte end_column){
		this.start = new Coordinate(start_row, start_column);
		this.end = new Coordinate(end_row, end_column);
	}
}
