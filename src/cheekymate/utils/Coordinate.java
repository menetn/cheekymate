package cheekymate.utils;

public class Coordinate {
	public byte row;// white starts at row 6/7, black starts at row 0/1
	public byte column;

	public Coordinate(int row, int column) {
		this.row = (byte) row;
		this.column = (byte) column;
	}

	public Coordinate(byte row, byte column) {
		this.row = row;
		this.column = column;
	}

	public boolean equals(Coordinate c) {
		return this.column == c.column && this.row == c.row;
	}
	
	public boolean within_board() {
		return this.row >= 0 && this.row <= 7 && this.column >= 0 && this.column <= 7;
	}
}
