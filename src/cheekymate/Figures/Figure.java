package cheekymate.Figures;

import java.util.ArrayList;

import cheekymate.utils.Coordinate;
import cheekymate.utils.FigureType;
import cheekymate.utils.Move;

public abstract class Figure {
	protected Colour colour;
	protected double value;

	//important for deep copy
	public abstract Figure copy();
	
	//Important for comparison (three fold repetition)
	public abstract boolean same(Figure f);
	
	public Colour getColour() {
		return this.colour;
	}

	public double getValue() {
		return this.value;
	}
	
	//Similar to an abstract initialiser
	public static Figure getFigureInstance(FigureType type, Colour colour) {
		Figure f;
		switch (type) {
		case King:
			f = new King();
			f.colour = colour;
			f.value = 0;//Checkmate is treated separately anyways
			break;
		case Queen:
			f = new Queen();
			f.colour = colour;
			f.value = 9;//Checkmate is treated separately anyways
			break;
		case Bishop:
			f = new Bishop();
			f.colour = colour;
			f.value = 3;//Checkmate is treated separately anyways
			break;
		case Knight:
			f = new Knight();
			f.colour = colour;
			f.value = 3;//Checkmate is treated separately anyways
			break;
		case Rook:
			f = new Rook();
			f.colour = colour;
			f.value = 5;//Checkmate is treated separately anyways
			break;
		default:
			f = new Pawn();
			f.colour = colour;
			f.value = 1;//Checkmate is treated separately anyways
		}
		return f;
	}

	public abstract void isMoved();// Allows the rook and king and pawn to change their status (no castling, no
									// advancements in steps of two)

	public abstract ArrayList<Move> getMoves(Figure[][] board, Coordinate position);// Important! board is read-only.
																					// Ignores gamestate

	// The addition of canGetKing makes computation quicker, since not always all
	// moves are relevant
	public abstract boolean canGetKing(Figure[][] board, Coordinate position, Coordinate kingPosition);// Important!
																										// board is
																										// read-only

}
