package cheekymate.Figures;

import java.util.ArrayList;

import cheekymate.utils.Coordinate;
import cheekymate.utils.Move;

public class Pawn extends Figure {
	private boolean moved = false;
	private boolean doubleMove = false;
	
	@Override
	public Figure copy() {
		Pawn c = new Pawn();
		c.colour = this.colour;
		c.value = this.value;
		c.moved = this.moved;
		c.doubleMove = this.doubleMove;
		return c;
	}
	
	@Override
	public boolean same(Figure f) {
		return f instanceof Pawn && f.colour == this.colour && f.value == this.value && ((Pawn) f).moved == this.moved && ((Pawn) f).doubleMove == this.doubleMove;
	}
	
	@Override
	public ArrayList<Move> getMoves(Figure[][] board, Coordinate position) {
		ArrayList<Move> moves = new ArrayList<Move>();
		byte dr;
		if (this.colour == Colour.white)
			dr = -1;
		else// this.colour == Colour.black
			dr = 1;

		// straight one block (then also check for two blocks)
		Coordinate newPosition = new Coordinate(position.row + dr, position.column);
		if (newPosition.within_board() && board[newPosition.row][newPosition.column] == null) {
			moves.add(new Move(position, newPosition));
			// Straight two blocks
			newPosition = new Coordinate(position.row + 2 * dr, position.column);
			if (newPosition.within_board() && this.moved == false && board[newPosition.row][newPosition.column] == null)
				moves.add(new Move(position, newPosition));
		}

		// Diagonal right
		newPosition = new Coordinate(position.row + dr, position.column + 1);
		if (newPosition.within_board() && board[newPosition.row][newPosition.column] != null
				&& board[newPosition.row][newPosition.column].getColour() != this.colour)
			moves.add(new Move(position, newPosition));
		// Diagonal left
		newPosition = new Coordinate(position.row + dr, position.column - 1);
		if (newPosition.within_board() && board[newPosition.row][newPosition.column] != null
				&& board[newPosition.row][newPosition.column].getColour() != this.colour)
			moves.add(new Move(position, newPosition));
		
		//En passant right (impossible to have its own figure on final square, thus no need to check if free/opponent colour
		newPosition = new Coordinate(position.row + dr, position.column + 1);
		if (newPosition.within_board() && board[position.row][position.column+1] instanceof Pawn 
				&& ((Pawn) board[position.row][position.column+1]).doubleMove == true && board[position.row][position.column+1].getColour() != this.colour)
			moves.add(new Move(position, newPosition));
		//En passant left
		newPosition = new Coordinate(position.row + dr, position.column - 1);
		if (newPosition.within_board() && board[position.row][position.column-1] instanceof Pawn 
				&& ((Pawn) board[position.row][position.column-1]).doubleMove == true && board[position.row][position.column-1].getColour() != this.colour)
			moves.add(new Move(position, newPosition));
		
		return moves;
	}

	// Not always false, after hypothetical move king could take king => important
	// to evaluate checkmate
	@Override
	public boolean canGetKing(Figure[][] board, Coordinate position, Coordinate kingPosition) {
		byte dr;
		if (this.colour == Colour.white)
			dr = -1;
		else// this.colour == Colour.black
			dr = 1;

		// Diagonal right
		Coordinate newPosition = new Coordinate(position.row + dr, position.column + 1);
		if (newPosition.equals(kingPosition))
			return true;
		// Diagonal left
		newPosition = new Coordinate(position.row + dr, position.column - 1);
		if (newPosition.equals(kingPosition))
			return true;
		
		return false;
	}

	public void setDoubleMove() {
		this.doubleMove = true;
	}
	
	public void resetDoubleMove() {
		this.doubleMove = false;
	}
	
	@Override
	public void isMoved() {
		this.moved = true;
	}

}
