package cheekymate.Figures;

import java.util.ArrayList;

import cheekymate.utils.Coordinate;
import cheekymate.utils.Move;

public class Rook extends Figure{
	private boolean moved = false;
	
	@Override
	public Figure copy() {
		Rook c = new Rook();
		c.colour = this.colour;
		c.value = this.value;
		return c;
	}
	
	@Override
	public boolean same(Figure f) {
		return f instanceof Rook && f.colour == this.colour && f.value == this.value && ((Rook) f).moved == this.moved;
	}
	
	@Override
	public ArrayList<Move> getMoves(Figure[][] board, Coordinate position) {
		ArrayList<Move> moves = new ArrayList<Move>();
		for (byte dr = -1; dr <= 1; dr++) {
			for (byte dc = -1; dc <= 1; dc++) {
				if (dr == dc || dr == -dc)//removes empty move and diagonal moves
					continue;
				for (byte distance = 1; distance <= 7; distance++) {
					Coordinate newPosition = new Coordinate(position.row + distance*dr, position.column + distance*dc);
					if (newPosition.within_board() == false)
						break;//Cannot go further in that direction
					if (board[newPosition.row][newPosition.column] != null) {
						if (board[newPosition.row][newPosition.column].getColour() != this.colour)
							moves.add(new Move(position, newPosition));
						break;//Cannot go further in that direction
					}
					else//board[newPosition.row][newPosition.column] == null
						moves.add(new Move(position, newPosition));
				}
			}
		}
		return moves;
	}

	//Not always false, after hypothetical move king could take king => important to evaluate checkmate
	@Override
	public boolean canGetKing(Figure[][] board, Coordinate position, Coordinate kingPosition) {
		for (byte dr = -1; dr <= 1; dr++) {
			for (byte dc = -1; dc <= 1; dc++) {
				if (dr == dc || dr == -dc)//removes empty move and diagonal moves
					continue;
				for (byte distance = 1; distance <= 7; distance++) {
					Coordinate newPosition = new Coordinate(position.row + distance*dr, position.column + distance*dc);
					if (newPosition.within_board() == false)
						break;//Cannot go further in that direction
					if (board[newPosition.row][newPosition.column] != null) {
						if (newPosition.equals(kingPosition))
							return true;
						break;//Cannot go further in that direction
					}
				}
			}
		}
		return false;
	}
	
	public boolean getMoved() {
		return moved;
	}
	
	@Override
	public void isMoved() {
		this.moved = true;
	}

}
