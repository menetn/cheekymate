package cheekymate.Figures;

import java.util.ArrayList;

import cheekymate.utils.Coordinate;
import cheekymate.utils.Move;

public class King extends Figure {
	private boolean moved = false;
	
	@Override
	public Figure copy() {
		King c = new King();
		c.colour = this.colour;
		c.value = this.value;
		c.moved = this.moved;
		return c;
	}
	
	@Override
	public boolean same(Figure f) {
		return f instanceof King && f.colour == this.colour && f.value == this.value && ((King) f).moved == this.moved;
	}
	
	@Override
	public ArrayList<Move> getMoves(Figure[][] board, Coordinate position) {
		ArrayList<Move> moves = new ArrayList<Move>();
		for (byte dr = -1; dr <= 1; dr++) {
			for (byte dc = -1; dc <= 1; dc++) {
				if (dr == 0 && dc == 0)
					continue;
				Coordinate newPosition = new Coordinate(position.row + dr, position.column + dc);
				if (newPosition.within_board() == false)
					continue;
				
				if (board[newPosition.row][newPosition.column] == null || board[newPosition.row][newPosition.column].getColour() != this.colour)
					moves.add(new Move(position, newPosition));
			}
		}
		
		//On castling the king may not be attacked at any substep, the king and the rook may not have been moved and no pieces may lie in between
		if (this.moved == false) {
			//Castling kingside
			if (board[position.row][position.column+3] instanceof Rook) {
				Rook kingsideRook = (Rook) board[position.row][position.column+3];
				if (kingsideRook.getMoved() == false && board[position.row][position.column+1] == null && board[position.row][position.column+2] == null) {
					//Check if king would be check during any substep
					boolean check = false;
					for (byte i = 0; i < 8; i++) {
						for (byte j = 0; j < 8; j++) {
							Figure figure = board[i][j];
							if (check == false && figure != null && figure.getColour() != this.colour) {
								if (figure.canGetKing(board, new Coordinate(i, j), position) 
								 || figure.canGetKing(board, new Coordinate(i, j), new Coordinate(position.row, position.column+1)) 
							   	 || figure.canGetKing(board, new Coordinate(i, j), new Coordinate(position.row, position.column+2)))
									check = true;
							}
						}
					}
					if (check == false)
						moves.add(new Move(position, new Coordinate(position.row,position.column+2)));
				}
			}
			//Castling queenside
			if (board[position.row][position.column-4] instanceof Rook) {
				Rook queensideRook = (Rook) board[position.row][position.column-4];
				if (queensideRook.getMoved() == false && board[position.row][position.column-1] == null && board[position.row][position.column-2] == null && board[position.row][position.column-3] == null) {
					//Check if king would be check during any substep
					boolean check = false;
					for (byte i = 0; i < 8; i++) {
						for (byte j = 0; j < 8; j++) {
							Figure figure = board[i][j];
							if (check == false && figure != null && figure.getColour() != this.colour) {
								if (figure.canGetKing(board, new Coordinate(i, j), position) 
								 || figure.canGetKing(board, new Coordinate(i, j), new Coordinate(position.row, position.column-1)) 
							   	 || figure.canGetKing(board, new Coordinate(i, j), new Coordinate(position.row, position.column-2)))
									check = true;
							}
						}
					}
					if (check == false)
						moves.add(new Move(position, new Coordinate(position.row,position.column-2)));
				}
			}
		}
		return moves;
	}

	//Not always false, after hypothetical move king could take king => important to evaluate checkmate
	@Override
	public boolean canGetKing(Figure[][] board, Coordinate position, Coordinate kingPosition) {
		for (byte dr = -1; dr <= 1; dr++) {
			for (byte dc = -1; dc <= 1; dc++) {
				if (dr == 0 && dc == 0)
					continue;
				Coordinate newPosition = new Coordinate(position.row + dr, position.column + dc);
				if (newPosition.equals(kingPosition))
					return true;
			}
		}
		return false;
	}

	public boolean getMoved() {
		return moved;
	}
	
	@Override
	public void isMoved() {
		this.moved = true;
	}

}
