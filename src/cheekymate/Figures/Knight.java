package cheekymate.Figures;

import java.util.ArrayList;

import cheekymate.utils.Coordinate;
import cheekymate.utils.Move;

public class Knight extends Figure{
	private static byte[][] offset = {{-1,-2},{-2,-1},{1,-2},{2,-1},{-1,2},{-2,1},{1,2},{2,1}};
	
	@Override
	public Figure copy() {
		Knight c = new Knight();
		c.colour = this.colour;
		c.value = this.value;
		return c;
	}
	
	@Override
	public boolean same(Figure f) {
		return f instanceof Knight && f.colour == this.colour && f.value == this.value;
	}
	
	@Override
	public ArrayList<Move> getMoves(Figure[][] board, Coordinate position) {
		ArrayList<Move> moves = new ArrayList<Move>();
		for (byte[] o : offset) {
			Coordinate newPosition = new Coordinate(position.row + o[0], position.column + o[1]);
			if (newPosition.within_board())
				if (board[newPosition.row][newPosition.column] == null || board[newPosition.row][newPosition.column].getColour() != this.colour)
					moves.add(new Move(position, newPosition));
		}
		return moves;
	}

	//Not always false, after hypothetical move king could take king => important to evaluate checkmate
	@Override
	public boolean canGetKing(Figure[][] board, Coordinate position, Coordinate kingPosition) {
		for (byte[] o : offset) {
			Coordinate newPosition = new Coordinate(position.row + o[0], position.column + o[1]);
			if (newPosition.equals(kingPosition))
				return true;
		}
		return false;
	}

	@Override
	public void isMoved() {}

}
