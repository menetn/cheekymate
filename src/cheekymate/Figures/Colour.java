package cheekymate.Figures;

public enum Colour {
	white,
	black;
	
	public static Colour other(Colour c) {
		if (c == Colour.white)
			return Colour.black;
		else
			return Colour.white;
		
	}
}
