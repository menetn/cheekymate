package cheekymate.Board;

import java.util.ArrayList;

import cheekymate.Figures.Bishop;
import cheekymate.Figures.Colour;
import cheekymate.Figures.Figure;
import cheekymate.Figures.King;
import cheekymate.Figures.Knight;
import cheekymate.Figures.Pawn;
import cheekymate.utils.Coordinate;
import cheekymate.utils.FigureType;
import cheekymate.utils.Move;

//does not recognize draw with several bishops of same colour, but since promotion is always a queen, this is of no concern
public class CheekyBoard implements ChessBoard {
	private Figure[][] board = new Figure[8][8];
	private double score;
	private ArrayList<Move> validMoves;
	private GameState state;
	private Colour colour;

	private Coordinate doubleMovedPawn = null;// Saves last double moved pawn, since en passant is only allowed directly
												// afterwards
	private int movesWithoutCaptureOrPawnMovement;// Important for fifty move rule

	private static Figure[][] deepcopyBoard(Figure[][] board) {
		Figure[][] copyBoard = new Figure[8][8];
		for (byte i = 0; i < 8; i++) {
			for (byte j = 0; j < 8; j++) {
				if (board[i][j] != null)
					copyBoard[i][j] = board[i][j].copy();
			}
		}
		return copyBoard;
	}

	public boolean equals(ChessBoard board) {
		boolean sameBoard = true;
		
		for (byte i = 0; i < 8; i++) {
			for (byte j = 0; j < 8; j++) {
				if (sameBoard == false)
					break;//prevents unnecessary repetitions
				
				Figure figureA = this.board[i][j];
				Figure figureB = board.getBoard()[i][j];
				
				if (figureA == figureB)//relevant if both are null
					continue;
				
				if (figureA != null && figureB != null) {
					if (figureA.same(figureB))
						continue;
				}
					
				sameBoard = false;
			}
		}
		
		return this.colour == board.getColour() && sameBoard;
	}
	
	public void setDraw() {
		this.state = GameState.draw;
	}
	
	public CheekyBoard(Figure[][] board, Colour colour, Coordinate doubleMovedPawn,
			int movesWithoutCaptureOrPawnMovement) throws Exception {
		this.board = deepcopyBoard(board);
		this.state = GameState.neutral;
		this.colour = colour;
		this.doubleMovedPawn = doubleMovedPawn;
		this.movesWithoutCaptureOrPawnMovement = movesWithoutCaptureOrPawnMovement;
		this.updateValidMovesAndGameState();
		this.updateScore();
	}

	@Override
	public Figure[][] getBoard() {
		return deepcopyBoard(this.board);
	}

	@Override
	public double getScore() {
		return score;
	}

	@Override
	public ArrayList<Move> getValidMoves() {
		return validMoves;
	}

	@Override
	public GameState getGameState() {
		return this.state;
	}

	@Override
	public Colour getColour() {
		return this.colour;
	}

	private void updateScore() {
		this.score = Score.computeScore(this);
	}

	@Override
	public boolean isValidMove(Move m) {
		for (Move move : this.validMoves) {
			if (move.start.row == m.start.row && move.start.column == m.start.column && move.end.row == m.end.row
					&& move.end.column == m.end.column)
				return true;
		}
		return false;
	}

	public void updateValidMovesAndGameState() throws Exception {
		this.validMoves = new ArrayList<Move>();

		// Get active King which might stand in check
		Coordinate activeKingCoordinate = null;
		for (byte i = 0; i < 8; i++) {
			for (byte j = 0; j < 8; j++) {
				Figure figure = board[i][j];
				if (figure != null && figure instanceof King && figure.getColour() == this.colour)
					activeKingCoordinate = new Coordinate(i, j);
			}
		}
		if (activeKingCoordinate == null)
			throw new Exception("Missing King");

		// Get all valid moves for both players and checks for check
		for (byte i = 0; i < 8; i++) {
			for (byte j = 0; j < 8; j++) {
				Figure figure = board[i][j];
				if (figure != null) {
					if (figure.getColour() == this.colour)
						this.validMoves.addAll(figure.getMoves(board, new Coordinate(i, j)));
					else {
						if (figure.canGetKing(this.board, new Coordinate(i, j), activeKingCoordinate)) {
							this.state = GameState.check;
						}
					}
				}
			}
		}
		
		// Remove moves after which the king is in check (as in when not moved out of
		// check or moved into check)
		ArrayList<Move> actualValidMoves = new ArrayList<Move>();
		for (Move m : this.validMoves) {
			// Hypothetical move, might lead to checkmate or expose the king for check
			Figure[][] new_board = deepcopyBoard(this.board);
			new_board[m.end.row][m.end.column] = new_board[m.start.row][m.start.column];
			new_board[m.start.row][m.start.column] = null;
			//Update king position if moved
			Coordinate new_activeKingCoordinate = activeKingCoordinate;
			if (new_board[m.end.row][m.end.column] instanceof King)
				new_activeKingCoordinate = new Coordinate(m.end.row, m.end.column);
				
			
			// Check if still/anew in check after move
			boolean allowed = true;
			for (byte i = 0; i < 8; i++) {
				for (byte j = 0; j < 8; j++) {
					Figure figure = new_board[i][j];
					if (allowed == true && figure != null && figure.getColour() != this.colour
							&& figure.canGetKing(new_board, new Coordinate(i, j), new_activeKingCoordinate)) {
						allowed = false;
					}
				}
			}
			if (allowed)
				actualValidMoves.add(m);
		}
		
		
		this.validMoves = actualValidMoves;

		// Updates GameState and check stalemate
		if (this.validMoves.isEmpty()) {
			if (this.state == GameState.check)
				this.state = GameState.checkmate;
			else if (this.state == GameState.neutral)
				this.state = GameState.draw;
		}
		
		//Verify if insufficient material is present. Does not recognize draw with several bishops of same colour, but since promotion is always a queen, this is of no concern
		byte whiteMaterial = 0;
		byte whiteBishops = 0;
		byte whiteKnights = 0;
		byte blackMaterial = 0;
		byte blackBishops = 0;
		byte blackKnights = 0;
		for (byte i = 0; i < 8; i++) {
			for (byte j = 0; j < 8; j++) {
				Figure figure = this.board[i][j];
				if (figure != null) {
					if (figure.getColour() == Colour.white) {
						whiteMaterial++;
						if (figure instanceof Bishop)
							whiteBishops++;
						if (figure instanceof Knight)
							whiteKnights++;
					}
					else {//figure.getColour() == Colour.black
						blackMaterial++;
						if (figure instanceof Bishop)
							blackBishops++;
						if (figure instanceof Knight)
							blackKnights++;
					}
				}
					
			}
		}
		boolean insufficientMaterial = (whiteMaterial == 1 && blackMaterial == 1) 
				|| (whiteMaterial == 2 && whiteKnights == 1 && blackMaterial == 1)
				|| (blackMaterial == 2 && blackKnights == 1 && whiteMaterial == 1)
				|| (whiteMaterial == 2 && whiteBishops == 1 && blackMaterial == 1)
				|| (blackMaterial == 2 && blackBishops == 1 && whiteMaterial == 1)
				|| (blackMaterial == 2 && blackBishops == 1 && whiteMaterial == 2 && whiteBishops == 1);
				
		//Check fifty move rule or insufficient material
		if (this.movesWithoutCaptureOrPawnMovement == 50 || insufficientMaterial) {
			this.validMoves = new ArrayList<Move>();
			this.state = GameState.draw;
		}
	}

	@Override
	public ChessBoard doMove(Move m) throws Exception {
		if (this.isValidMove(m)) {

			boolean captureFigureOrPawnMove = false;

			Figure[][] new_board = deepcopyBoard(this.board);
			if (this.doubleMovedPawn != null)// Resets double move for next board (newBoard.doubleMovedPawn == null anyways, except if another double move is made)
				((Pawn) new_board[this.doubleMovedPawn.row][this.doubleMovedPawn.column]).resetDoubleMove();
			if (new_board[m.end.row][m.end.column] != null)// Important for fifty move rule
				captureFigureOrPawnMove = true;
			new_board[m.end.row][m.end.column] = new_board[m.start.row][m.start.column];
			new_board[m.start.row][m.start.column] = null;
			new_board[m.end.row][m.end.column].isMoved();// Updates status of figure

			// Castling => adjust rook (move m relates to king's movement)
			if (this.board[m.start.row][m.start.column] instanceof King) {
				if (m.end.column == m.start.column + 2) {// Castle king side
					new_board[m.end.row][m.end.column - 1] = new_board[m.end.row][m.end.column + 1];
					new_board[m.end.row][m.end.column - 1].isMoved();
					new_board[m.end.row][m.end.column + 1] = null;
				}
				if (m.end.column == m.start.column - 2) {// Castle queen side
					new_board[m.end.row][m.end.column + 1] = new_board[m.end.row][m.end.column - 2];
					new_board[m.end.row][m.end.column + 1].isMoved();
					new_board[m.end.row][m.end.column - 1] = null;
				}
			}

			// Special pawn moves
			if (new_board[m.end.row][m.end.column] instanceof Pawn) {
				captureFigureOrPawnMove = true;
				// Double move, enables en passant
				if (m.start.row == m.end.row + 2 || m.start.row == m.end.row - 2) {
					((Pawn) new_board[m.end.row][m.end.column]).setDoubleMove();
					return new CheekyBoard(new_board, Colour.other(this.colour),
							new Coordinate(m.end.row, m.end.column), (byte) 0);
				}

				// En passant (only happens when diagonal move occurs and pawn lands on
				// preoccupied space)
				else if (m.start.column != m.end.column && this.board[m.end.row][m.end.column] == null) {
					new_board[m.start.row][m.end.column] = null;
				}

				// Pawn queen promotion
				else if (m.end.row == 0) {
					Colour pawn_colour = new_board[m.end.row][m.end.column].getColour();
					new_board[m.end.row][m.end.column] = Figure.getFigureInstance(FigureType.Queen, pawn_colour);
				} else if (m.end.row == 7) {
					Colour pawn_colour = new_board[m.end.row][m.end.column].getColour();
					new_board[m.end.row][m.end.column] = Figure.getFigureInstance(FigureType.Queen, pawn_colour);
				}
			}

			return new CheekyBoard(new_board, Colour.other(this.colour), null,
					captureFigureOrPawnMove ? 0 : this.movesWithoutCaptureOrPawnMovement + 1);
		} else
			return null;
	}

}
