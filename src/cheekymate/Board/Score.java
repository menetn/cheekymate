package cheekymate.Board;

import cheekymate.Figures.Colour;
import cheekymate.Figures.Figure;

public class Score {
	public static double computeScore(ChessBoard cb) {
		Figure[][] board = cb.getBoard();
		if (cb.getGameState() == GameState.checkmate)
			return (cb.getColour() == Colour.white ? 1 : -1)*Double.MIN_VALUE;
		if (cb.getGameState() == GameState.draw)
			return 0;
		
		double score = 0;
		// Count figure score
		for (byte i = 0; i < 8; i++) {
			for (byte j = 0; j < 8; j++) {
				Figure figure = board[i][j];
				if (figure != null)
					score += (figure.getColour() == Colour.white ? 1 : -1) * figure.getValue();
			}
		}
		
		return score;
	}
}
