package cheekymate.Board;

import java.util.ArrayList;

import cheekymate.Figures.Colour;
import cheekymate.Figures.Figure;
import cheekymate.utils.Move;

public interface ChessBoard {
	
	public double getScore();
	public Figure[][] getBoard();
	public GameState getGameState();
	public Colour getColour();
	
	public void setDraw();
	
	public boolean equals(ChessBoard board);
	
	public ChessBoard doMove(Move m) throws Exception;//Returns null if cannot do move. Throws exception if something breaks (bug).
	public boolean isValidMove(Move m);
	public ArrayList<Move> getValidMoves();
}