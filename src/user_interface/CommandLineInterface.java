package user_interface;

import java.util.Scanner;

import cheekymate.Main;
import cheekymate.Figures.Bishop;
import cheekymate.Figures.Colour;
import cheekymate.Figures.Figure;
import cheekymate.Figures.King;
import cheekymate.Figures.Knight;
import cheekymate.Figures.Pawn;
import cheekymate.Figures.Queen;
import cheekymate.Figures.Rook;
import cheekymate.utils.Move;
import cheekymate.Board.ChessBoard;
import cheekymate.Board.GameState;

public class CommandLineInterface {
	
	private static Scanner s = new Scanner(System.in);

	static char getCharacter(Figure f) {
		if (f == null)
			return '·';
		Colour colour = f.getColour();
		if (f instanceof King) {
			if (colour == Colour.white)
				return '♔';
			else
				return '♚';
		}
		else if (f instanceof Queen) {
			if (colour == Colour.white)
				return '♕';
			else
				return '♛';
		}
		else if (f instanceof Bishop) {
			if (colour == Colour.white)
				return '♗';
			else
				return '♝';
		}
		else if (f instanceof Knight) {
			if (colour == Colour.white)
				return '♘';
			else
				return '♞';
		}
		else if (f instanceof Rook) {
			if (colour == Colour.white)
				return '♖';
			else
				return '♜';
		}
		else if (f instanceof Pawn) {
			if (colour == Colour.white)
				return '♙';
			else
				return '♟';
		}
		return 'E';//error since Figure could not be assigned
	}
	
	static byte columnForChar(char c) throws Exception{
		switch (c) {
		case 'a':
			return 0;
		case 'b':
			return 1;
		case 'c':
			return 2;
		case 'd':
			return 3;
		case 'e':
			return 4;
		case 'f':
			return 5;
		case 'g':
			return 6;
		case 'h':
			return 7;
		}
		throw new Exception("Invalid column input");
	}
	
	static char charForColumn(byte c) throws Exception{
		switch (c) {
		case 0:
			return 'a';
		case 1:
			return 'b';
		case 2:
			return 'c';
		case 3:
			return 'd';
		case 4:
			return 'e';
		case 5:
			return 'f';
		case 6:
			return 'g';
		case 7:
			return 'h';
		}
		throw new Exception("Cannot convert column to char");
	}
	
	static byte rowForChar(char c) throws Exception{
		switch (c) {
		case '8':
			return 0;
		case '7':
			return 1;
		case '6':
			return 2;
		case '5':
			return 3;
		case '4':
			return 4;
		case '3':
			return 5;
		case '2':
			return 6;
		case '1':
			return 7;
		}
		throw new Exception("Invalid row input");
	}
	
	static char charForRow(byte r) throws Exception{
		switch (r) {
		case 0:
			return '8';
		case 1:
			return '7';
		case 2:
			return '6';
		case 3:
			return '5';
		case 4:
			return '4';
		case 5:
			return '3';
		case 6:
			return '2';
		case 7:
			return '1';
		}
		throw new Exception("Cannot convert row to char");
	}
	
	public static void display(ChessBoard cb) throws Exception{
		Figure[][] board = cb.getBoard();
		
		System.out.print('\n');
		System.out.print('%');
		for (byte column = 0; column <= 7; column++) {//Top border
			System.out.print(charForColumn(column));
		}
		System.out.print('%');
		System.out.print('\n');
		for (byte row = 0; row <= 7; row++) {
			System.out.print(charForRow(row));//Left border
			for (byte column = 0; column <= 7; column++) {
				char c = getCharacter(board[row][column]);
				System.out.print(c);//Board tiles
			}
			System.out.print(charForRow(row));//Right border
			System.out.print('\n');
		}
		System.out.print('%');
		for (byte column = 0; column <= 7; column++) {//Bottom border
			System.out.print(charForColumn(column));
		}
		System.out.print('%');
		System.out.println('\n');
		
		switch (cb.getGameState()) {
		case check:
			System.out.println("Check");
			break;
		case checkmate:
			System.out.println("Checkmate");
			break;
		case draw:
			System.out.println("Draw");
			break;
		default:
			break;
		}
		if (cb.getColour() == Colour.white)
			System.out.println("White's turn");
		else
			System.out.println("Black's turn");
	}
	
	public static Move readMove() {
		System.out.println("Enter a move in the following format: a2 a4");
		String input = s.nextLine();
		String[] splitted = input.split(" ");
		
		if (splitted.length != 2 || splitted[0].length() != 2 || splitted[1].length() != 2) {
			System.out.println("Invalid format");
			return readMove();
		}
		try{
			byte start_column = columnForChar(splitted[0].charAt(0));
			byte start_row = rowForChar(splitted[0].charAt(1));
			byte end_column = columnForChar(splitted[1].charAt(0));
			byte end_row = rowForChar(splitted[1].charAt(1));
			return new Move(start_row, start_column, end_row, end_column);
		}
		catch(Exception e){
			System.out.println("Invalid format");
			return readMove();
		}
		
	}
	
}
