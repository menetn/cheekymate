package user_interface;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.awt.*;

import javax.imageio.ImageIO;
import javax.swing.*;

import javax.swing.JFrame;
import javax.swing.JMenu;

import cheekymate.GameController;
import cheekymate.Main;
import cheekymate.Figures.Bishop;
import cheekymate.Figures.Colour;
import cheekymate.Figures.Figure;
import cheekymate.Figures.King;
import cheekymate.Figures.Knight;
import cheekymate.Figures.Pawn;
import cheekymate.Figures.Queen;
import cheekymate.Figures.Rook;
import cheekymate.utils.Coordinate;
import cheekymate.utils.Move;

public class Window {
	
	private final JFrame gameFrame;
	private final BoardPanel boardPanel;
	
	private final static Dimension OUTER_FRAME_DIMENSION = new Dimension(600,600);
	private final static Dimension TILE_PANEL_DIMENSION = new Dimension(50,50);
	private static String defaultFigureImagesPath = "src/pictures/";
	
	private Coordinate firstCoordinate = null;
	
	private GameController gc;
	
	private boolean interactive = true;
	
	
	public Window(GameController gc) {
		this.gc = gc;
		
		this.gameFrame = new JFrame("CheekyChess");
		this.gameFrame.setLayout(new BorderLayout());
		final JMenuBar chessboardMenuBar = createChessboardMenuBar();
		this.gameFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		this.gameFrame.setJMenuBar(chessboardMenuBar);
		this.gameFrame.setSize(OUTER_FRAME_DIMENSION);
		
		this.boardPanel = new BoardPanel(this);
		this.gameFrame.add(this.boardPanel, BorderLayout.CENTER);
		this.gameFrame.setVisible(true);
	}
	
	public void setInteractive(boolean v) {
		this.interactive = v;
	}
	
	public void updateDisplay() throws IOException {
		Figure [][] board = gc.chessBoardHistory.get(gc.chessBoardHistory.size() - 1).getBoard();
		for (TilePanel row : this.boardPanel.boardTiles) {
			row.assignFigure(board);
		}
	}
	
	public void processClick(int tileId){
		if (!interactive)//Disables interaction (e.g. when computer calculates)
			return;
		
		if (firstCoordinate == null)
			firstCoordinate = new Coordinate(tileId/8, tileId%8);
		else {
			Coordinate secondCoordinate = new Coordinate(tileId/8, tileId%8);
			Move m = new Move(firstCoordinate, secondCoordinate);
			firstCoordinate = null;
			this.boardPanel.resetBackground();
			try {
				if(gc.tryMove(m) == true) {//successful move
					this.interactive = false;
					updateDisplay();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private JMenuBar createChessboardMenuBar() {
		final JMenuBar chessboardMenuBar = new JMenuBar();
		chessboardMenuBar.add(createFileMenu());
		return chessboardMenuBar;
	}
	private JMenu createFileMenu() {
		
		final JMenu fileMenu = new JMenu("File");
		
		final JMenuItem newGame = new JMenuItem("New Game");
		newGame.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					Main.init();
					gameFrame.setVisible(false); //you can't see me!
					gameFrame.dispose();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		fileMenu.add(newGame);
		
		final JMenuItem exitMenuItem= new JMenuItem("Exit");
		exitMenuItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
			
		});
		fileMenu.add(exitMenuItem);
		
		return fileMenu;
	}
	
	
	//Board
	private class BoardPanel extends JPanel{
		Window window;
		final List<TilePanel> boardTiles;
		
		BoardPanel(Window window){
			super(new GridLayout(8,8));
			this.window = window;
			this.boardTiles = new ArrayList<>();
			for(int i=0;i<64;i++) {
				final TilePanel tilePanel = new TilePanel(this, i, window);
				this.boardTiles.add(tilePanel);
				add(tilePanel);
			}
		}
		
		void resetBackground() {
			for(int i=0;i<64;i++) {
				this.boardTiles.get(i).assignTileColor();;
			}
		}
		
	}
	
	//Square
	private class TilePanel extends JPanel {
		Figure figure = null;
		
		Window window;
		
		private final int tileId;
		BufferedImage img=null;
		
		TilePanel(final BoardPanel boardPanel, final int tileId, Window window){
			super(new GridBagLayout());
			this.window = window;
			this.tileId=tileId;
			setPreferredSize(TILE_PANEL_DIMENSION);
			assignTileColor();
			
			addMouseListener(new MouseAdapter() {

                @Override
                public void mousePressed(MouseEvent e){
                	if (window.interactive) {
	                    setBackground(Color.GREEN);
	                    window.processClick(tileId);
                	}
                }
			});
		}
		
		void assignFigure(final Figure[][] board) {
			int i = this.tileId/8;
			int j = this.tileId%8;
			this.figure = board[i][j];
		}
		
		@Override
	    protected void paintComponent(Graphics g) {
	        super.paintComponent(g);
	        try {
				updateFigureIcon();
			} catch (IOException e) {
				e.printStackTrace();
			}
	        g.drawImage(this.img, 0, 0, this.getWidth(), this.getHeight(), this);          
	        validate();
	        repaint();
		}
		
		public void updateFigureIcon() throws IOException {
			
			if(figure instanceof Bishop && figure.getColour()==Colour.white) {
				this.img=ImageIO.read(new File(defaultFigureImagesPath + "WB.png"));
			}
			else if(figure instanceof King && figure.getColour()==Colour.white) {
				this.img=ImageIO.read(new File(defaultFigureImagesPath + "WK.png"));
			}
			else if(figure instanceof Knight && figure.getColour()==Colour.white) {
				this.img=ImageIO.read(new File(defaultFigureImagesPath + "WN.png"));
			}
			else if(figure instanceof Pawn && figure.getColour()==Colour.white) {
				this.img=ImageIO.read(new File(defaultFigureImagesPath + "WP.png"));
			}
			else if(figure instanceof Queen && figure.getColour()==Colour.white) {
				this.img=ImageIO.read(new File(defaultFigureImagesPath + "WQ.png"));
			}
			else if(figure instanceof Rook && figure.getColour()==Colour.white) {
				this.img=ImageIO.read(new File(defaultFigureImagesPath + "WR.png"));
			}
			else if(figure instanceof Bishop && figure.getColour()==Colour.black) {
				this.img=ImageIO.read(new File(defaultFigureImagesPath + "BB.png"));
			}
			else if(figure instanceof King && figure.getColour()==Colour.black) {
				this.img=ImageIO.read(new File(defaultFigureImagesPath + "BK.png"));
			}
			else if(figure instanceof Knight && figure.getColour()==Colour.black) {
				this.img=ImageIO.read(new File(defaultFigureImagesPath + "BN.png"));
			}
			else if(figure instanceof Pawn && figure.getColour()==Colour.black) {
				this.img=ImageIO.read(new File(defaultFigureImagesPath + "BP.png"));
			}
			else if(figure instanceof Queen && figure.getColour()==Colour.black) {
				this.img=ImageIO.read(new File(defaultFigureImagesPath + "BQ.png"));
			}
			else if(figure instanceof Rook && figure.getColour()==Colour.black) {
				this.img=ImageIO.read(new File(defaultFigureImagesPath + "BR.png"));
			}
			else
				this.img = null;
			
		}

		private void assignTileColor() {
			if((this.tileId%8 + this.tileId/8)%2==0) {
				setBackground(Color.decode("#FFFACD"));
			} else {
				setBackground(Color.decode("#593E1A"));
			}
			
		}
	}
	
}
