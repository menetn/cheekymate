package computer;

import java.util.concurrent.TimeoutException;

import cheekymate.GameController;
import cheekymate.Board.ChessBoard;
import cheekymate.Board.GameState;
import cheekymate.Board.Score;
import cheekymate.utils.Coordinate;
import cheekymate.utils.Move;

public class Computer extends Thread{
	private ChessBoard chessboard = null;
	private GameController gc = null;
	private long timeLimit;
	private long timeStart;

	
	//time limit should be given in milliseconds
	public void init(GameController gc, long timeLimit) {
		this.gc = gc;
		this.chessboard = gc.chessBoardHistory.get(gc.chessBoardHistory.size() - 1);
		this.timeLimit = timeLimit;
	}
	
	@Override
	public void run() {
		while (true) {
			try {
				Move m = this.calculateMove();
				if(gc.tryComputerMove(m))//Exits loop if success
					return;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public Move calculateMove() throws Exception{
		this.timeStart = System.currentTimeMillis();
		Move currBestMove = null;
		for (int i = 1;;i++) {//depth
			try {
				currBestMove = bestMove(i);
				System.out.println("Achieved search depth of " + i);
			}
			catch (TimeoutException e){
				break;
			}
		}
		return currBestMove;
	}
	
	private Move bestMove(int maxDepth) throws Exception{
		Move bestMove = null;
		double beta = Double.MAX_VALUE;
		chessboard.getBoard();
		for (Move m : chessboard.getValidMoves()) {
			checkTime();
			ChessBoard newBoard = chessboard.doMove(m);
			
			double candidateScore = min(newBoard, 1, maxDepth, Double.MIN_VALUE, Double.MAX_VALUE);
			if (candidateScore < beta) {
				bestMove = m;
				beta = candidateScore;
			}
		}
		return bestMove;
	}

	private double max(ChessBoard cb, int depth, int maxDepth, double alpha, double beta) throws Exception{
		if (depth == maxDepth || cb.getGameState() == GameState.checkmate || cb.getGameState() == GameState.draw)//Leaf
			return Score.computeScore(cb);
		
		for (Move m : cb.getValidMoves()) {
			checkTime();
			if (alpha >= beta)//Cut off unnecessary branches
				return alpha;
			
			ChessBoard newBoard = cb.doMove(m);

			double candidateScore = min(newBoard, depth+1, maxDepth, alpha, beta);
			if (candidateScore > alpha) {
				alpha = candidateScore;
			}
		}
		
		if (alpha == Double.MIN_VALUE)//Leaf (no moves)
			alpha = Score.computeScore(cb);
		
		return alpha;
	}
	
	private double min(ChessBoard cb, int depth, int maxDepth, double alpha, double beta) throws Exception{
		if (depth == maxDepth || cb.getGameState() == GameState.checkmate || cb.getGameState() == GameState.draw)
			return Score.computeScore(cb);

		for (Move m : cb.getValidMoves()) {
			checkTime();
			
			if (alpha >= beta)//Cut off unnecessary branches
				return beta;
			
			ChessBoard newBoard = cb.doMove(m);

			double candidateScore = max(newBoard, depth+1, maxDepth, alpha, beta);
			if (candidateScore < beta) {
				beta = candidateScore;
			}
		}

		if (beta == Double.MAX_VALUE)//Leaf (no moves)
			beta = Score.computeScore(cb);
		return beta;
	}
	
	void checkTime() throws TimeoutException{
		if (this.timeStart + this.timeLimit < System.currentTimeMillis())//Check time constraints
			throw new TimeoutException("The Minmax Algorithm consumed too much time. Aborting to ensure fair play.");
	}
}
